Feature: Validate login functionality.
	
Scenario Outline: this is for success login            
     Given navigate to login page <url>
     When  user is navigated to lopin page
     Then  user enters details <email> <password>
     And   click login button
     Then  user will navigate to homepage with logged in name
     And   click on logout button
     Then  user will navigate to homepage with logged out
     And   close the browser
     
     Examples:
    | url | email | password |
    | "https://demowebshop.tricentis.com/login" | "harshit22@yopmail.com" | "1234..Hart" |
    | "https://demowebshop.tricentis.com/login" | "harshit23@yopmail.com" | "1234..Hart" |